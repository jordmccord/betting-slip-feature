'use strict';

var $ = require('jquery'),
    Backbone = require('backbone');

module.exports = Backbone.Model.extend({
    url: 'https://bedefetechtest.herokuapp.com/v1/bets',
    idAttribute: 'bet_id',
    defaults: {
        return: 0,
        stake: 0
    },
    bet(bet){
        var deferred = $.Deferred();
        this.clear();
        this.save(bet,{
            type: 'post',
            success(model, response, options) {
                deferred.resolve(model, response, options);
            },
            error(model, response, options){
                deferred.reject(model, response, options);
            }
        });

        return deferred.promise();
    }
});