'use strict';

var Backbone = require('Backbone'),
    ReceiptModel = require('../models/receipt');

module.exports = Backbone.Collection.extend({
    model: ReceiptModel
});