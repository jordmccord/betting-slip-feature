'use strict';

var $ = require('jquery'),
    Backbone = require('Backbone'),
    BetModel = require('../models/bet');

module.exports = Backbone.Collection.extend({
    model: BetModel,
    url: 'https://bedefetechtest.herokuapp.com/v1/markets',
    load(){
        var deferred = $.Deferred();
        this.fetch({
            success(collection, response, options) {
                deferred.resolve(collection, response, options);
            },
            error(collection, response, options){
                deferred.reject(collection, response, options);
            }
        });

        return deferred.promise();
    }
});