"use strict";

var Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    Shim = require('./radio.shim'),
    AppRouter = require('./routes/router');

// Initialise the application
var App = new Marionette.Application();

// Add the region to the app
App.addRegions({
    'main': '#app'
});

// Initialise the router
var router = new AppRouter({
    App: App
});

// Listen for the start event then start BB history
App.on('start', () => {
    Backbone.history.start();
});

// Start the application
App.start();