'use strict';

var Marionette = require('backbone.marionette'),
    BetView = require('../views/bet'),
    BetsTemplate = require('../templates/bets.hbs');

module.exports = Marionette.CompositeView.extend({
    className: 'markets',
    template: BetsTemplate,
    childView: BetView,
    childViewContainer: '#bets-container'
});